//
//  Flashcard.swift
//  PortableFL (this name is a joke)
//
//  Created by Sean Wong on 12/12/19.
//  Copyright © 2019 Tinkertanker. All rights reserved.
//

import Foundation
import UIKit
class Flashcard: Codable {
    
    var title: String
    var definition: String
    var exampleSentence: String
    var imageURL: URL

    init(title: String, description: String, exampleSentence: String, imageURL: URL) {
        self.title = title
        self.definition = description
        self.exampleSentence = exampleSentence
        self.imageURL = imageURL
    }
    
    // These functions were taken from YJ
    static func getArchiveURL() -> URL {
        let plistName = "flashcards"
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        return documentsDirectory.appendingPathComponent(plistName).appendingPathExtension("plist")
    }
    static func saveToFile(flashcards: [Flashcard]) {
        let archiveURL = getArchiveURL()
        let propertyListEncoder = PropertyListEncoder()
        let encodedResults = try? propertyListEncoder.encode(flashcards)
        try? encodedResults?.write(to: archiveURL, options: .noFileProtection)
    }
    static func loadFromFile() -> [Flashcard]? {
        let archiveURL = getArchiveURL()
        let propertyListDecoder = PropertyListDecoder()
        guard let retrievedResultData = try? Data(contentsOf: archiveURL) else { return nil }
        guard let decodedResults = try? propertyListDecoder.decode(Array<Flashcard>.self, from: retrievedResultData) else { return nil }
        return decodedResults
    }
    
}
